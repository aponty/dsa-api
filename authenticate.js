exports.verifyUser = (req, res, next) => {
    // mockup for token (or cookie, but tokens are better imo) authentication middleware
    // I normally use json web tokens through passport
    // Given the recommended time limit I'm going to skip for the moment
    if (req){
        // get token off req, verify, next() or nope()
        console.log('authenticated')
        next();
    } else {
        const err = new Error('You are not authorized to perform this operation');
        err.status = 403 ;
        next(err);
    }
};

