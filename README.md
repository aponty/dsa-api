# DSA Mock Api

run `npm install` to load libraries

start with `npm start`

Then, open Postman/CURL/your request handler of choice

## Query existing users: 
GET http://localhost:3000/users

Takes queries in standard querystring format;  

- the relevant url,  
- a "?" indicating the start of the query,   
- and a series of key-value pairs joined with "&"  
```
http://url?column_name={value}&next_column_name={value}
```

ex:  
```
http://localhost:3000/users?first_name=Garek&last_name=Baselio&chapter_name=Muzi&first_sorted=true
```
search by:  
- first_name={string}  
- last_name={string}  
- chapter_name={string}  

sort by first name:
- first_sorted=true

sort by second name:
- second_sorted=true

returns array of matching users

## Create a new user: 
POST http://localhost:3000/users  
include new user as JSON in body of request.  
If using Postman, select "raw" and "JSON" and paste the following for an example;  
```
{
    "first_name": "newUserFirstName",
    "last_name": "newUserLastName",
    "email": "newUserEmail",
    "profile_image": "fillerString",
    "dues_paid": "true",
    "last_dues_payment": "fillerDate",
    "chapter_name": "newChapter"
}
```

returns the newly created user

## Update/edit a user:  
PUT http://localhost:3000/users/{id_of_user_to_edit}  
Include edits to user as JSON in body of request, per above.   
Include or omit fields as necessary.

PUT http://localhost:3000/users/501
```
{
    "first_name": "editedName",
    "email": "editedEmail"
}
```
returns the edited user; 

## Delete a user: 
DELETE http://localhost:3000/users/{id_of_user_to_delete}  
returns the id of the deleted user
