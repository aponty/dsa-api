const express = require("express");
const usersRouter = express.Router();
const User = require('../models/User');
const authenticate = require("../authenticate");

usersRouter.route("/")
.get(authenticate.verifyUser, (req, res, next) => {
  User
    .findAll({
      where: User.whereQuery(req.query),
      order: User.sortQuery(req.query),
    })
    .then((users) => res.json(users))
    .catch((err) => next(err));
})
.post(authenticate.verifyUser, (req, res, next) => {
  User
    .create(req.body)
    .then((users) => res.json(users))
    .catch((err) => next(err));
})
.put((_, res) => {
  res.statusCode = 403;
  res.end("PUT operation not supported on /users");
})
.delete((_, res) => {
  res.statusCode = 403;
  res.end("DELETE operation not supported on /users");
});


usersRouter.route("/:userID")
.get(authenticate.verifyUser, (req, res, next) => {
  User
    .findOne({ where: { id: req.params.userID } })
    .then((user) => {
      if (user) {
        res.json(user);
      } else {
        res.statusCode = 400;
        res.end(`No user by that id exists`);
      }
    })
    .catch((err) => next(err));
})
.post((req, res) => {
  res.statusCode = 403;
  res.end(`POST operation not supported on /users/${req.params.userID}
  Did you  mean to PUT? Or POST a new user to /users`);
})
.put(authenticate.verifyUser, (req, res, next) => {
  User
    .findOne({ where: { id: req.params.userID } })
    .then((user) => {
      if (user) {
        user.update(req.body).then((updatedUser) => res.json(updatedUser));
      } else {
        const err = new Error("No user by that id to update");
        err.status = 400;
        next(err);
      }
    })
    .catch((err) => next(err));
})
.delete(authenticate.verifyUser, (req, res, next) => {
  User
    .findOne({ where: { id: req.params.userID } })
    .then((user) => {
      if (user) {
        user.destroy().then(_ => res.json(user.id));
      } else {
        const err = new Error("No user by that id to delete");
        err.status = 400;
        next(err);
      }
    })
    .catch((err) => next(err));
});

module.exports = usersRouter;
