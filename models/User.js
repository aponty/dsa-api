const { Sequelize, DataTypes } = require("sequelize");
const sequelize = require("../db/database");

class User extends Sequelize.Model {
  static sortQuery = (query) => {
    // create array of possible query options,
    // remove undefined clauses
    const opts = [
      query.first_sorted && ["first_name", "ASC"],
      query.second_sorted && ["last_name", "ASC"],
    ];
    return opts.filter(Boolean);
  };

  static whereQuery = (query) => {
    // create hash of possible query options,
    // remove undefined clauses
    const whereQuery = {
      first_name: query.first_name,
      last_name: query.last_name,
      chapter_name: query.chapter_name,
    };
    
    Object.keys(whereQuery).forEach((key) => 
      whereQuery[key] === undefined && delete whereQuery[key]);
    
    return whereQuery;
  };
}

// left all data types as strings to keep the import simple for time's sake
User.init({
  first_name: { type: DataTypes.STRING },
  last_name: { type: DataTypes.STRING },
  email: { type: DataTypes.STRING },
  profile_image: { type: DataTypes.STRING },
  dues_paid: { type: DataTypes.STRING },
  last_dues_payment: { type: DataTypes.STRING },
  chapter_name: { type: DataTypes.STRING },
},{
  sequelize,
  modelName: "user_data",
});

module.exports = User;
