const Sequelize = require("sequelize");
const DBSOURCE = "./db/data.sqlite";

const sequelize = new Sequelize({
  dialect: "sqlite",
  storage: DBSOURCE,
  define: {
    timestamps: false,
    freezeTableName: true,
  },
});

sequelize.sync().then(() => console.log(`Database & tables connected`));

module.exports = sequelize;
